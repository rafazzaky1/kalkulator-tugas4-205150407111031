package com.example.kalkulator_tugas4_205150407111031;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView solutionTv;
    MaterialButton buttonC;
    MaterialButton buttonDivide, buttonMultiply, buttonPlus, buttonMinus, buttonEquals;
    MaterialButton button1, button2, button3, button4, button5, button6, button7, button8, button9, button0;
    MaterialButton buttonDot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        solutionTv = findViewById(R.id.input);

        assignId(buttonC,R.id.buttonC);
        assignId(buttonDivide,R.id.buttondiv);
        assignId(buttonMultiply,R.id.buttonmul);
        assignId(buttonPlus,R.id.buttonadd);
        assignId(buttonMinus,R.id.buttonsub);
        assignId(buttonEquals,R.id.button_equal);
        assignId(button1,R.id.button1);
        assignId(button2,R.id.button2);
        assignId(button3,R.id.button3);
        assignId(button4,R.id.button4);
        assignId(button5,R.id.button5);
        assignId(button6,R.id.button6);
        assignId(button7,R.id.button7);
        assignId(button8,R.id.button8);
        assignId(button9,R.id.button9);
        assignId(button0,R.id.button0);
        assignId(buttonDot,R.id.button10);




    }



    void assignId(MaterialButton btn, int id){
        btn = findViewById(id);
        btn.setOnClickListener(this);

    }
    @Override
    public void onClick(View view) {

        MaterialButton button =(MaterialButton) view;
        String buttonText = button.getText().toString();
        String dataToCalculate = solutionTv.getText().toString();
        if(dataToCalculate.startsWith("0."))
        {
            dataToCalculate = "0.";
        }
        else if(dataToCalculate.startsWith("0")){
            dataToCalculate = "";
        }

        if (buttonText.equals("AC")){
            solutionTv.setText("");
            return;
        }

        if(buttonText.equals("C")){
            dataToCalculate = dataToCalculate.substring(0,dataToCalculate.length()-1);
        }
        else if(buttonText.equals("="))
        {
            dataToCalculate = dataToCalculate;
        }
        else{
            dataToCalculate = dataToCalculate+buttonText;
        }

        solutionTv.setText(dataToCalculate);
        String finalResult = getResult(dataToCalculate);

        if(buttonText.equals("=")){
            Intent intent = new Intent(this, com.example.kalkulator_tugas4_205150407111031.MainActivity2.class);
            intent.putExtra("finalResult",finalResult);
            intent.putExtra("solution",dataToCalculate);
            startActivity(intent);
        }

    }
    String getResult(String data){
        try {
            Context context = Context.enter();
            context.setOptimizationLevel(-1);
            Scriptable scriptable = Context.enter().initStandardObjects();
            String finalResult = context.evaluateString(scriptable, data, "Javascript", 1, null).toString();
            if (finalResult.endsWith(".0")){
                finalResult = finalResult.replace(".0", "");
            }
            return finalResult;
        }catch (Exception e){
            return "error";

        }

    }

}
